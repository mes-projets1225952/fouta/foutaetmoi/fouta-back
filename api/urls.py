from django.urls import path
from . import views

urlpatterns = [
#Liens SITE
    path('site/', views.SiteListView.as_view(), name='site'),
    path('site/ajout/', views.SiteCreateView.as_view(), name='Ajout_site'),
    path('site/edit/<int:id>/', views.SiteUpdateView.as_view(), name='editS'),
    # path('site/delete/<int:pk>/', views.SiteDetail.as_view(), name='deleteS'),
    path('site/<int:pk>/', views.SiteDetail.as_view(), name='detail'),

#Liens Hotel
    path('hotel/', views.HotelListView.as_view(), name="hotel"),
    path('hotel/ajout/', views.HotelCreateView.as_view(), name="createHotel"),
    path('hotel/<int:pk>/', views.HotelDetail.as_view(), name="detail"),
    path('hotel/delete/<int:id>', views.HotelDeleteView.as_view(), name="deleteH"),
#Liens GUIDE
    path('guide/', views.GuideListView.as_view(), name="guide"),
    path('guide/ajout/', views.GuideCreateView.as_view(), name="createG"),
    path('guide/<int:pk>/', views.GuideDetail.as_view(), name="editeG"),
    path('guide/deleteG/<int:id>', views.GuideDeleteView.as_view(), name="deleteG"),
]