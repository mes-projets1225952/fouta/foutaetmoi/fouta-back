from rest_framework import serializers

from myApp.models import *


class SiteSerializer(serializers.ModelSerializer):
    class Meta:
        model= Site
        fields=('id_site','nom','ville','type','photo')

class HotelSerializer(serializers.ModelSerializer):
    class Meta:
        model=Hotel
        fields=('id_hotel','nom','type','ville','photo')

class GuideSerializer(serializers.ModelSerializer):
    class Meta:
        model=Guide
        fields=('id_guide','nom','mail','tel','age','site_T')
