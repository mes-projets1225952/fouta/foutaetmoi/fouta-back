import site

from django.contrib import messages
from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse_lazy
from django.views.generic import TemplateView, ListView, CreateView, UpdateView, DeleteView, DetailView
from rest_framework import status
from rest_framework.generics import CreateAPIView, ListAPIView, UpdateAPIView, DestroyAPIView, RetrieveDestroyAPIView, \
    RetrieveAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.response import Response
from rest_framework import  generics

from myApp.models import *
from api.serializers import *

# Create .
class SiteCreateView(CreateAPIView):
    serializer_class = SiteSerializer
    # model = Site
    # template_name = 'pages/ajout_site.html'
    # context_object_name = 'site'
    # fields=['nom','type','ville','photo']
    # success_url = reverse_lazy("site")
    # def form_valid(self, form):
    #     messages.success(self.request,"Enregistrement réussi")
    #     return super(SiteCreateView,self).form_valid(form)


class HotelCreateView(CreateAPIView):
    # model = Hotel
    # template_name = 'pages/ajout_hotel.html'
    # context_object_name = 'hotels'
    # fields = ['nom','type']
    # success_url = reverse_lazy("hotel")
    # def form_valid(self, form):
    #     messages.success(self.request,"Enregistrement réussi")
    #     return super(HotelCreateView,self).form_valid(form)
    serializer_class = HotelSerializer

class GuideCreateView(CreateAPIView):
    # model = Guide
    # template_name = 'pages/ajout_guide.html'
    # success_url = reverse_lazy('guide')
    # context_object_name = 'site'
    # def form_valid(self, form):
    #     messages.success(self.request,"Enregistrement réussi")
    #     return super(GuideCreateView,self).form_valid(form)
    serializer_class = GuideSerializer
# Read
class SiteListView(ListAPIView):
    serializer_class = SiteSerializer
    # model=Site
    # template_name='pages/site.html'
    # context_object_name = 'site'
    def get_queryset(self):
        queryset = Site.objects.all()
        return queryset
class HotelListView(ListAPIView):
     serializer_class = HotelSerializer
     # template_name = 'pages/hotel.html'
     # context_object_name = 'hotels'
     def get_queryset(self):
        queryset=Hotel.objects.all()
        return queryset;
class GuideListView(ListAPIView):
    serializer_class = GuideSerializer
    # template_name = 'pages/guide.html'
    # context_object_name = 'guide'
    def get_queryset(self):
        queryset = Guide.objects.all()
        return queryset
# class SiteDetailView(DetailView):
#     model = Site
#     template_name = "pages/detailS.html"
#     context_object_name = 'site '
#     def get_context_data(self, **kwargs):
#         context = super(SiteDetailView, self).get_context_data(**kwargs)
#         print(self.kwargs['pk'])
#         try:
#             context['site'] = Site.objects.get(id=self.kwargs['pk'])
#         except:
#             redirect("site")
#         return context
# Update
class SiteUpdateView(UpdateAPIView):
    # template_name = 'pages/editS.html'
    # model = Site
    # pk_url_kwarg = 'id'
    # success_url = reverse_lazy('site')
    # fields = ["nom",'type','ville','photo']

    serializer_class = SiteSerializer
    queryset = Site.objects.all()
    #
    def get_object(self):
         if self.request.method == 'PUT':
             site = Site.objects.filter(id=self.kwargs.get('id')).first()
             if site:
                 return site
             else:
                 return Hotel(id=self.kwargs.get('id'))
         else:
            return super(SiteSerializer, self).get_object()


# Site by id, get, update, delete site
class HotelDetail(RetrieveUpdateDestroyAPIView):
    queryset = Hotel.objects.all
    serializer_class = HotelSerializer
    def get_queryset(self):
        return Hotel.objects.all()
class SiteDetail(RetrieveUpdateDestroyAPIView):
    queryset = Site.objects.all
    serializer_class = SiteSerializer
    def get_queryset(self):
        return Site.objects.all()
class GuideDetail(RetrieveUpdateDestroyAPIView):
    queryset = Guide.objects.all
    serializer_class = GuideSerializer
    def get_queryset(self):
        return Guide.objects.all()

class HotelUpdateView(UpdateView):
    # template_name = 'pages/edit_h.html'
    # model = Hotel
    # pk_url_kwarg = 'id'
    # success_url =reverse_lazy('hotel')
    # fields = ['nom','type']
    serializer_class = HotelSerializer
    queryset = Hotel.objects.all()
    #
    def get_object(self):
         if self.request.method == 'PUT':
             hotel = Hotel.objects.filter(id=self.kwargs.get('id')).first()
             if hotel:
                 return hotel
             else:
                 return Hotel(id=self.kwargs.get('id'))
         else:
             return super(HotelSerializer, self).get_object()
class GuideUpdateView(UpdateAPIView, ListAPIView):
    # template_name = 'pages/editG.html'
    # #context_object_name = 'sites'
    # model = Guide
    # success_url = reverse_lazy('guide')
    # fields = ['nom', 'tel', 'mail', 'age', 'site_T']
    serializer_class = GuideSerializer
    queryset = Guide.objects.all()
    def get_object(self):
         if self.request.method == 'PUT':
             guide = Guide.objects.filter(id=self.kwargs.get('id')).first()
             if guide:
                 return guide
             else:
                 return Guide(id=self.kwargs.get('id'))
         else:
             return super(GuideSerializer, self).get_object()

# Delete
class SiteDeleteView(DestroyAPIView):
    # template_name = "pages/deleteS.html"
    # model = Site
    # success_url = reverse_lazy("site")
    # pk_url_kwarg = 'id'
    serializer_class = SiteSerializer
    queryset = Site.objects.all()

    def get_site(self, id):
         try:
             return Site.objects.get(id=id)
         except:
             return None

    def delete(self, request, id):
         site = self.get_site(id)
         if site == None:
             return Response(status=status.HTTP_404_NOT_FOUND)
         site.delete()
         return Response("Suppression effectuée")
class HotelDeleteView(DestroyAPIView):
    # template_name = "pages/delete_h.html"
    # model=Hotel
    # success_url = reverse_lazy("hotel")
    # pk_url_kwarg = 'id'
    serializer_class = HotelSerializer
    queryset = Hotel.objects.all()
    def get_hotel(self, id):
         try:
             return Hotel.objects.get(id=id)
         except:
             return None
    def delete(self, request, id):
         hotel = self.get_hotel(id)
     #         return Response({"status": "fail", "message": f"Note with Id: {id} not found"}, status=status.HTTP_404_NOT_FOUND)
    #
         hotel.delete()
         return Response(status=status.HTTP_204_NO_CONTENT)
class GuideDeleteView(DestroyAPIView):
    # model = Guide
    # success_url = reverse_lazy("guide")
    # template_name = "pages/deleteG.html"
    # pk_url_kwarg = 'id'
    serializer_class = GuideSerializer
    queryset = Guide.objects.all()
    def get_guide(self, id):
        try:
             return Guide.objects.get(id=id)
        except:
             return None

    def delete(self, request, id):
         guide = self.get_guide(id)
         if guide == None:
             return Response({"status": "fail", "message": f"Note with Id: {id} not found"},
                             status=status.HTTP_404_NOT_FOUND)

         guide.delete()
         return Response("Suppression effectuée")

