from django.contrib import admin

from .models import*

# Register your models here.
admin.site.register(Site,Site_admins)
admin.site.register(Guide,Guide_admins)
admin.site.register(Hotel,Hotel_admins)

