from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from django.template import loader
from .models import *
def home(request):
    return render(request,"pages/home.html")

def about(request):
    return render(request,"pages/about.html")

def admin(request):
    return render(request,"/admin")
# READ
def site(request):
    return render(request, 'pages/site.html', {'site': Site.objects.all()})

def hotel(request):
    return render(request, 'pages/hotel.html', {'hotels': Hotel.objects.all()})

def guide(request):
    return render(request, 'pages/guide.html', {'guide': Guide.objects.all(),'site':Site.objects.all()})
def show(request, id):
    site = get_object_or_404(Site, id=id)
    return render(request, "pages/detailS.html", {'site' :site})

# CREATE
def ajoutHotel(request):
    if (request.method == "POST"):
        hotel = Hotel.objects.create(
            nom=request.POST["nom"],
            type=request.POST["type"],
            )
        hotel.save()
        return redirect("/hotel/")
    return render(request, "pages/ajout_hotel.html")

def ajout_site(request):
    if(request.method=="POST"):
        site=Site.objects.create(
            nom=request.POST["nom"],
            ville=request.POST["ville"],
            type=request.POST["type"],
            photo=request.FILES["photo"]
        )
        site.save()
        return redirect('/site/')
    return render(request, "pages/ajout_site.html")

def ajout_G(request):
    site = Site.objects.all()
    if (request.method=="POST"):
        guide=Guide.objects.create(
            nom=request.POST["nom"],
            age=request.POST["age"],
            tel =request.POST["tel"],
            mail = request.POST["mail"],
            site_T=Site.objects.get(pk=request.POST["site_T"])
        )
        guide.save()
        return redirect("/guide/")
    return render(request,"pages/ajout_guide.html", {"site":site})

#UPDATE
def editH(request, id):
    hotel = get_object_or_404(Hotel, id=id)
    if (request.method == "POST"):
        hotel = Hotel.objects.filter(id=hotel.id).update(
            nom=request.POST["nom"],
            type=request.POST["type"],
        )
        return redirect("/hotel/")
    return render(request, "pages/edit_h.html", {'hotel': hotel})

def editG(request, id):
    guide=get_object_or_404(Guide,id=id)
    sites = Site.objects.all()
    if (request.method=="POST"):
        guide=Guide.objects.filter(id=guide.id).update(
            nom=request.POST["nom"],
            age=request.POST["age"],
            tel=request.POST["tel"],
            mail=request.POST["mail"],
            site_T=Site.objects.get(pk=request.POST["site_T"])
        )
        return redirect('/guide/')
    return render(request,"pages/editG.html", {"guide": guide,"sites":sites})

def editS(request, id):
    site=get_object_or_404(Site,id=id)
    if (request.method == "POST"):
        site = Site.objects.get(id=site.id)
        if site:
            site.nom=request.POST["nom"]
            site.ville=request.POST["ville"]
            site.type=request.POST["type"]
            site.photo=request.FILES["photo"]
            site.save()


        return redirect('/site/')
    return render(request,"pages/editS.html",{"site":site})

# DELETE
def deleteH(request, id):
    hotel = get_object_or_404(Hotel, id=id)
    if (request.method == "POST"):
        hotel = Hotel.objects.filter(pk=hotel.id).delete()
        return redirect("/hotel/")
    return render(request, 'pages/delete_h.html', {'hotel': hotel})

def deleteG(request, id):
    guide=get_object_or_404(Guide,id=id)
    if (request.method=="POST"):
        guide=Guide.objects.filter(pk=guide.id).delete()
        return redirect('/guide/')
    return render(request,"pages/deleteG.html",{"guide":guide})

def deleteS(request,id):
    site=get_object_or_404(Site,id=id)
    if (request.method=="POST"):
        site=Site.objects.filter(pk=site.id).delete()
        return redirect("/site/")
    return render(request, "pages/deleteS.html", {"site":site})




# Create your views here.
