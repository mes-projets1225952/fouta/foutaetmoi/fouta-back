from django.db import models
from django.contrib import admin
# Create your models here.

class Hotel(models.Model):
    id_hotel= models.AutoField(primary_key=True)
    nom=models.CharField(max_length=30,unique=True)
    type=models.CharField(max_length=50)
    photo=models.ImageField(upload_to='media')
    ville=models.CharField(max_length=30)

class Site(models.Model):
    id_site = models.AutoField(primary_key=True)
    nom=models.CharField(max_length=50)
    ville=models.CharField(max_length=20)
    type=models.CharField(max_length=30)
    photo=models.ImageField(upload_to='media')


class Guide(models.Model):
    id_guide = models.AutoField(primary_key=True)
    nom=models.CharField(max_length=30)
    age=models.IntegerField()
    tel=models.IntegerField()
    mail=models.EmailField()
    photo = models.ImageField(upload_to='media')
    site_T=models.ForeignKey(Site,on_delete=models.CASCADE)

class Hotel_admins(admin.ModelAdmin):
    list_display = ('id_hotel','nom','type','photo','ville')

class Guide_admins(admin.ModelAdmin):
    list_display = ('id_guide','nom','age','tel','mail','site_T')

class Site_admins(admin.ModelAdmin):
    list_display = ('id_site','nom', 'ville','type','photo')
